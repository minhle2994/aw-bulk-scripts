var apiBaseUrl = "http://gman-awdataingest.spy126.com"

var managerAccount = AdsApp.currentAccount()
Logger.log("Manger account: %s - %s", managerAccount.getCustomerId(), managerAccount.getName())

AdsManagerApp.select(managerAccount)
var accountIterator = AdsManagerApp.accounts().get()
Logger.log("Number of managed ads accounts: %s", accountIterator.totalNumEntities())

while (accountIterator.hasNext()) {
  var account = accountIterator.next()
  Logger.log("Account: %s - %s", account.getName(), account.getCustomerId())
  AdsManagerApp.select(account)
  
  var campaignIterator = AdsApp.shoppingCampaigns().get()
  while (campaignIterator.hasNext()) {
    var campaign = campaignIterator.next()
    var adGroupIterator = campaign.adGroups().get()
    while (adGroupIterator.hasNext()) {
      var adGroup = adGroupIterator.next()
      var keywords = getScheduledExcludeKeywords(adGroup.getId())
      if (keywords.length > 0) {
        for (var i = 0; i < keywords.length; i++) {
          Logger.log("Add negative keyword %s (%s) | Campaign %s - %s | Ad group %s - %s", 
                     getNegativeKeywordText(keywords[i]), keywords[i], campaign.getId().toString(), campaign.getName(), adGroup.getId().toString(), adGroup.getName())
          adGroup.createNegativeKeyword(getNegativeKeywordText(keywords[i]))
        }
      }
    }
  }
}


function getScheduledExcludeKeywords(adGroupId) {
  var options = {
    "method" : "get",
    "headers": {"Authorization": "Basic " + Utilities.base64Encode("gman:nggsKNPgP7aQAZwp")}
  }
  var url = apiBaseUrl + "/get_scheduled_exclude_keywords?apiKey=" + apiKey + "&adGroupId=" + adGroupId
  var response = UrlFetchApp.fetch(url, options)
  if (response.getResponseCode() === 200) {
    return JSON.parse(response.getContentText())
  } else {
    Logger.log('Invalid status code: %s', response.getResponseCode())
    return []
  }
}

function getNegativeKeywordText(keyword) {
  if (keyword.matchType === "broad")
    return keyword.searchTerms
  else if (keyword.matchType === "phrase")
    return '"' + keyword.searchTerms + '"'
  else if (keyword.matchType === "exact")
    return "[" + keyword.searchTerms + "]"
}
