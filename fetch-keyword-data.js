var dateRangeValues = [
  "TODAY", 
  "YESTERDAY", 
  "LAST_7_DAYS", 
  "THIS_WEEK_SUN_TODAY", 
  "LAST_WEEK", 
  "LAST_14_DAYS", 
  "LAST_30_DAYS", 
  "LAST_BUSINESS_WEEK", 
  "LAST_WEEK_SUN_SAT", 
  "THIS_MONTH", 
  "LAST_MONTH", 
  "ALL_TIME",
]

var keywordFields = [
  "Query",
  "QueryTargetingStatus",
  "QueryMatchTypeWithVariant",
  "CampaignId",
  "CampaignName",
  "CampaignStatus",
  "AdGroupId",
  "AdGroupName",
  "AdGroupStatus",
  "Cost",
  "Impressions",
  "Interactions",
  "InteractionRate",
  "Conversions",
  "ConversionRate",
  "CostPerConversion",
  "ValuePerConversion",
]

var apiBaseUrl = "http://gman-awdataingest.spy126.com"
var managerAccount = AdsApp.currentAccount()
Logger.log("Manger account: %s - %s", managerAccount.getCustomerId(), managerAccount.getName())
AdsManagerApp.select(managerAccount)
var accountIterator = AdsManagerApp.accounts().get()
Logger.log("Number of managed ads accounts: %s", accountIterator.totalNumEntities())

while (accountIterator.hasNext()) {
  var account = accountIterator.next()
  AdsManagerApp.select(account)
  var keywordData = prepareKeywordData(account)
  // Logger.log("Keyword data: %s", JSON.stringify(keywordData))
  sendKeywordData(keywordData)
}

function sendKeywordData(data) {
  var options = {
    "method" : "post",
    "contentType": "application/json",
    "payload" : JSON.stringify(data),
    "headers": {"Authorization": "Basic " + Utilities.base64Encode("gman:nggsKNPgP7aQAZwp")}
  }
  UrlFetchApp.fetch(apiBaseUrl + "/update_keyword", options)
}

function buildKeywordReportQuery(dateRange, whereCondition) {
  var query = "SELECT " + keywordFields.join(", ") + " FROM SEARCH_QUERY_PERFORMANCE_REPORT" + " WHERE " + whereCondition
  if (dateRange != "ALL_TIME")
    query += " DURING " + dateRange
  return query
}

function prepareKeywordData(account) {
  var data = {}
  var dateRange = "LAST_7_DAYS"
  var whereConditions = [
    "Cost > 0 AND Conversions > 0", 
    "Cost > 1000000", 
    "Interactions > 10"
  ]
  data[dateRange] = []
  for (var i=0; i<3; i++) {
    cond = whereConditions[i]
    var report = AdsApp.report(buildKeywordReportQuery(dateRange, cond))
    var rows = report.rows()
    while (rows.hasNext())
      data[dateRange].push(rows.next().formatForUpload())
  }

  data["awAccountId"] = account.getCustomerId()
  data["apiKey"] = apiKey
  return data
}
