var dateRangeValues = [
  "TODAY", 
  "YESTERDAY", 
  "LAST_7_DAYS", 
  "THIS_WEEK_SUN_TODAY", 
  "LAST_WEEK", 
  "LAST_14_DAYS", 
  "LAST_30_DAYS", 
  "LAST_BUSINESS_WEEK", 
  "LAST_WEEK_SUN_SAT", 
  "THIS_MONTH", 
  "LAST_MONTH", 
  "ALL_TIME",
]

var productFields = [
  "OfferId",
  "StoreId",
  "CampaignName",
  "MerchantId",
  "ProductTitle",
  "Impressions",
]

var apiBaseUrl = "http://gman-awdataingest.spy126.com"
var managerAccount = AdsApp.currentAccount()
Logger.log("Manger account: %s - %s", managerAccount.getCustomerId(), managerAccount.getName())
AdsManagerApp.select(managerAccount)
var accountIterator = AdsManagerApp.accounts().get()
Logger.log("Number of managed ads accounts: %s", accountIterator.totalNumEntities())

while (accountIterator.hasNext()) {
  var account = accountIterator.next()
  AdsManagerApp.select(account)
  var productData = prepareProductData(account)
  // Logger.log("Product data: %s", JSON.stringify(productData))
  sendProductData(productData)
}


function sendProductData(data) {
  var options = {
    "method" : "post",
    "contentType": "application/json",
    "payload" : JSON.stringify(data),
    "headers": {"Authorization": "Basic " + Utilities.base64Encode("gman:nggsKNPgP7aQAZwp")}
  }
  UrlFetchApp.fetch(apiBaseUrl + "/update_aw_product", options)
}

function buildProductReportQuery(dateRange) {
  var query = "SELECT " + productFields.join(", ") + " FROM SHOPPING_PERFORMANCE_REPORT"
  if (dateRange != "ALL_TIME")
    query += " DURING " + dateRange
  return query
}

function prepareProductData(account) {
  var data = {}
  var dateRange = "LAST_7_DAYS"
  data[dateRange] = []
  var report = AdsApp.report(buildProductReportQuery(dateRange))
  var rows = report.rows()
  while (rows.hasNext())
    data[dateRange].push(rows.next().formatForUpload())

  Logger.log("Aw account: %s | Number of product has impressions: %s", account.getCustomerId(), data[dateRange].length)
  data["awAccountId"] = account.getCustomerId()
  data["apiKey"] = apiKey
  return data
}
