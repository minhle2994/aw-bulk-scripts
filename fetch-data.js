var dateRangeValues = [
  "TODAY", 
  "YESTERDAY", 
  "LAST_7_DAYS", 
  "THIS_WEEK_SUN_TODAY", 
  "LAST_WEEK", 
  "LAST_14_DAYS", 
  "LAST_30_DAYS", 
  "LAST_BUSINESS_WEEK", 
  "LAST_WEEK_SUN_SAT", 
  "THIS_MONTH", 
  "LAST_MONTH", 
  "ALL_TIME",
]

var adGroupFields = [
  "AdGroupId",
  "AdGroupName",
  "CampaignName",
  "CampaignStatus",
  "AdGroupStatus",
  "BiddingStrategyType",
  "CpcBid",
  "CpmBid",
  "EffectiveTargetRoas",
  "Cost",
  "Impressions",
  "Clicks",
  "Conversions",
  "AverageCpm",
  "AverageCpc",
  "CostPerConversion",
  "ValuePerConversion",
  "Ctr",
  "ConversionRate",
]

var apiBaseUrl = "http://gman-awdataingest.spy126.com"

var managerAccount = AdsApp.currentAccount()
Logger.log("Manger account: %s - %s", managerAccount.getCustomerId(), managerAccount.getName())
AdsManagerApp.select(managerAccount)
var accountIterator = AdsManagerApp.accounts().get()
Logger.log("Number of managed ads accounts: %s", accountIterator.totalNumEntities())

while (accountIterator.hasNext()) {
  var account = accountIterator.next()
  AdsManagerApp.select(account)
  
  var accountData = prepareAccountData(account)
  Logger.log("Account data: %s", accountData)
  sendAccountData(accountData)

  var adGroupData = prepareAdGroupData(account)
  // Logger.log("AdGroup data: %s", adGroupData)
  sendAdGroupData(adGroupData)
}

function sendAdGroupData(data) {
  var options = {
    "method" : "post",
    "contentType": "application/json",
    "payload" : JSON.stringify(data),
    "headers": {"Authorization": "Basic " + Utilities.base64Encode("gman:nggsKNPgP7aQAZwp")}
  }
  UrlFetchApp.fetch(apiBaseUrl + "/update_aw_ad_group", options)
}

function buildAdGroupsReportQuery(dateRange) {
  var query = "SELECT " + adGroupFields.join(", ") + " FROM ADGROUP_PERFORMANCE_REPORT"
  if (dateRange != "ALL_TIME")
    query += " DURING " + dateRange
  return query
}

function prepareAdGroupData(account) {
  var data = {}
  for (var i = 0; i < dateRangeValues.length; i++) {
    var dateRange = dateRangeValues[i]
    var report = AdsApp.report(buildAdGroupsReportQuery(dateRange))
    var rows = report.rows()
    data[dateRange] = []
    while (rows.hasNext())
      data[dateRange].push(rows.next().formatForUpload())
  }
  data["awAccountId"] = account.getCustomerId()
  data["apiKey"] = apiKey
  return data
}
  
function sendAccountData(data) {
  var options = {
    "method" : "post",
    "contentType": "application/json",
    "payload" : JSON.stringify(data),
    "headers": {"Authorization": "Basic " + Utilities.base64Encode("gman:nggsKNPgP7aQAZwp")}
  }
  UrlFetchApp.fetch(apiBaseUrl + "/update_aw_account", options)
}

function getAccountSpendingLimit(account) {
  var budgetOrderIterator = AdsApp.budgetOrders().withCondition('status="ACTIVE"').get()
  if (budgetOrderIterator.hasNext()) {
    var budgetOrder = budgetOrderIterator.next()
    if (budgetOrder.getSpendingLimit() == null)
      return -1
    else
      return budgetOrder.getSpendingLimit()
  } else {
    Logger.log("Account %s don't have any active budget order", account.getCustomerId())
    return null
  }
}

function getAccountCost(account) {
  return account.getStatsFor("ALL_TIME").getCost()
}

function prepareAccountData(account) {
  var data = {
    "apiKey": apiKey,
    "customerId": account.getCustomerId(),
    "name": account.getName(),
    "currencyCode": account.getCurrencyCode(),
    "spendingLimit": getAccountSpendingLimit(account),
    "totalCost": getAccountCost(account),
    "timeZone": account.getTimeZone(),
  }
  return data
}
